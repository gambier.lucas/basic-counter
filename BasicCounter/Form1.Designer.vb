﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MinusButton = New System.Windows.Forms.Button()
        Me.PlusButton = New System.Windows.Forms.Button()
        Me.RAZButton = New System.Windows.Forms.Button()
        Me.LblTotal = New System.Windows.Forms.Label()
        Me.Total = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'MinusButton
        '
        Me.MinusButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!)
        Me.MinusButton.Location = New System.Drawing.Point(132, 186)
        Me.MinusButton.Name = "MinusButton"
        Me.MinusButton.Size = New System.Drawing.Size(150, 50)
        Me.MinusButton.TabIndex = 0
        Me.MinusButton.Text = "-"
        Me.MinusButton.UseVisualStyleBackColor = True
        '
        'PlusButton
        '
        Me.PlusButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!)
        Me.PlusButton.Location = New System.Drawing.Point(490, 186)
        Me.PlusButton.Name = "PlusButton"
        Me.PlusButton.Size = New System.Drawing.Size(150, 50)
        Me.PlusButton.TabIndex = 1
        Me.PlusButton.Text = "+"
        Me.PlusButton.UseVisualStyleBackColor = True
        '
        'RAZButton
        '
        Me.RAZButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!)
        Me.RAZButton.Location = New System.Drawing.Point(303, 296)
        Me.RAZButton.Name = "RAZButton"
        Me.RAZButton.Size = New System.Drawing.Size(150, 50)
        Me.RAZButton.TabIndex = 2
        Me.RAZButton.Text = "RAZ"
        Me.RAZButton.UseVisualStyleBackColor = True
        '
        'LblTotal
        '
        Me.LblTotal.AutoSize = True
        Me.LblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.LblTotal.Location = New System.Drawing.Point(356, 115)
        Me.LblTotal.Name = "LblTotal"
        Me.LblTotal.Size = New System.Drawing.Size(44, 20)
        Me.LblTotal.TabIndex = 3
        Me.LblTotal.Text = "Total"
        '
        'Total
        '
        Me.Total.AutoSize = True
        Me.Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!)
        Me.Total.Location = New System.Drawing.Point(355, 197)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(26, 29)
        Me.Total.TabIndex = 4
        Me.Total.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Total)
        Me.Controls.Add(Me.LblTotal)
        Me.Controls.Add(Me.RAZButton)
        Me.Controls.Add(Me.PlusButton)
        Me.Controls.Add(Me.MinusButton)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MinusButton As Button
    Friend WithEvents PlusButton As Button
    Friend WithEvents RAZButton As Button
    Friend WithEvents LblTotal As Label
    Friend WithEvents Total As Label
End Class
