﻿Imports ClassLibraryCounter
Public Class Form1
    Public Compt As Counters
    Private Sub PlusButton_Click(sender As Object, e As EventArgs) Handles PlusButton.Click
        Compt.Plus()
        Total.Text = Compt.GetValue()
    End Sub

    Private Sub MinusButton_Click(sender As Object, e As EventArgs) Handles MinusButton.Click
        Compt.Moins()
        Total.Text = Compt.GetValue()
    End Sub

    Private Sub RAZButton_Click(sender As Object, e As EventArgs) Handles RAZButton.Click
        Compt.Raz()
        Total.Text = Compt.GetValue()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Compt = New Counters()
        Total.Text = Compt.GetValue()
    End Sub
End Class