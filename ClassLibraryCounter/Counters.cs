﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryCounter
{
    public class Counters
    {
        private int Total;

        public Counters()
        {
            this.Total = 0;
        }

        public int GetValue()
        {
            return this.Total;
        }

        public void Plus()
        {
            this.Total = this.Total + 1;
        }

        public void Moins()
        {
            if (GetValue() > 0)
            {
                this.Total = this.Total - 1;
            }
        }

        public void Raz()
        {
            this.Total = 0;
        }
    }
}