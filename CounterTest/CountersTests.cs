﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibraryCounter;

namespace CounterTest
{
    [TestClass]
    public class CountersTests
    {
        [TestMethod]
        public void GetValue_Test()
        {
            Counters Counter = new Counters();
            Assert.AreEqual(0, Counter.GetValue());
        }
        [TestMethod]
        public void Plus_Test()
        {
            Counters Counter = new Counters();
            Counter.Plus();
            Assert.AreEqual(1, Counter.GetValue());
        }
        [TestMethod]
        public void Moins_Test()
        {
            Counters Counter = new Counters();
            Counter.Plus();
            Counter.Plus();
            Counter.Moins();
            Assert.AreEqual(1, Counter.GetValue());
            Counters Counter2 = new Counters();
            Counter.Moins();
            Assert.AreEqual(0, Counter.GetValue());
        }
        [TestMethod]
        public void RAZ_Test()
        {
            Counters Counter = new Counters();
            Counter.Plus();
            Counter.PLus();
            Counter.Moins();
            Counter.Raz();
            Assert.AreEqual(0, Counter.GetValue());
        }
    }
}